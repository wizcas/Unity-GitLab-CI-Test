#!/bin/sh
UNITY_PATH="/Applications/Unity/Unity.app/Contents/MacOS/Unity"
WORKSPACE=`pwd`
RESULT_PATH=$WORKSPACE"/TestResults/unittest.xml"
LOG_PATH=$WORKSPACE"/TestResults/UnitTestEditor.log"

$UNITY_PATH -batchmode -projectPath $WORKSPACE -runEditorTests -editorTestsResultFile $RESULT_PATH -logFile $LOG_PATH -nographics
RETURN=$?

if [ $RETURN -ne 3 ];then
    if [ $RETURN -eq 0 ];then
        echo "\033[32mAll test passed\033[0m\n"
    else
        echo "\033[31mSome tests are failed. Please check the test result. ($RETURN)\033[0m\n"
    fi
    echo "Test result is saved at "$RESULT_PATH
else
    echo "\033[31mFailed to run any test. Please check the full log.\033[0m"
fi
echo "Full log is saved at "$LOG_PATH
exit $RETURN