#!/bin/sh
UNITY_PATH="/Applications/Unity/Unity.app/Contents/MacOS/Unity"
WORKSPACE=`pwd`
RESULTS_DIR=$WORKSPACE"/TestResults"
LOG_PATH=$WORKSPACE"/TestResults/IntegrationTestEditor.log"
$UNITY_PATH -batchmode -projectPath $WORKSPACE -executeMethod UnityTest.Batch.RunIntegrationTests -resultsFileDirectory=$RESULTS_DIR -logFile $LOG_PATH #-testscenes=IntegrationTests,IntegrationTests1,IntegrationTests2 #-targetPlatform=Android,iOS,StandaloneWindows,StandaloneOSXUniversal
RETURN=$?

if [ $RETURN -ne 3 ];then
    if [ $RETURN -eq 0 ];then
        echo "\033[32mAll test passed\033[0m\n"
    else
        echo "\033[31mSome tests are failed. Please check the test results. ($RETURN)\033[0m\n"
    fi
    echo "Test results are saved at "$RESULTS_DIR
else
    echo "\033[31mFailed to run any test. Please check the full log.\033[0m"
fi
echo "Full log is saved at "$LOG_PATH
exit $RETURN