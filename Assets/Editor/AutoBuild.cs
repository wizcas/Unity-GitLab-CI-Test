﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Linq;

public class AutoBuild {

    [MenuItem("AutoBuild/Build")]
    public static void Build() {
        string[] scenes = EditorBuildSettings.scenes.Select(s => s.path).ToArray();
        BuildPipeline.BuildPlayer(scenes, "test.apk", BuildTarget.Android, BuildOptions.None);
    }

    [MenuItem("Unity Test Tools/Run Integration Tests")]
    public static void RunIntegrationTests() {
        var scenes = EditorBuildSettings.scenes;
        Debug.Log(string.Join(", ", scenes.Select(s=>s.path).ToArray()));
//        UnityTest.IntegrationTests.PlatformRunner.BuildAndRunInPlayer(new PlatformRunnerConfiguration(BuildTarget.StandaloneOSXIntel64));
        UnityTest.Batch.RunIntegrationTests();
//        UnityTest.Batch.RunIntegrationTests(null, new System.Collections.Generic.List<string>
//            {
//                "IntegrationTests1",
//                "IntegrationTests2"
//            }, new System.Collections.Generic.List<string>());
    }

//    public static void RunAllTests(){
//        UnityEditor.EditorTests.Batch.RunTests();
//        UnityTest.Batch.RunIntegrationTests();
//    }
}
